import multiprocessing
from aliot.aliot_obj import AliotObj
from stream_utils import Stream
from car_utils import Car
from time import sleep


car = Car()
carALiot = AliotObj("car")
ip_address = Stream.get_ip_address()

linkStream = "http://" + ip_address + ":8080/video"

def start():
   while True:
        x, y = car.sendPosition()
        try:
            carALiot.update_doc({
                "/doc/voiture": [{
                    "x": x,
                    "y": y,
                    "id": 1,
                    "speed": 0,
                    "linkStream": linkStream,
                    "streamState": True
                }]
            })
            sleep(1)
        except KeyboardInterrupt:
            break


carALiot.on_start(callback=start)
carALiot.run()
